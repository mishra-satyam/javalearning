# JAVA LEARNING NOTES

## SOME GENERAL POINTS
- running java package files
```bash
javac -d . <FileName.java>
.....

java <package>.<mainFileName>
```


- **Instance variables** are variables declared inside a class but outside any method.
- **Local variables** are variables declared inside a method or method parameter.
- All local variables live on the stack, in the frame corresponding to the method where the variables are declared.
- All objects live in the heap, regardless of whether the reference is a local or instance variable.
- **MATH CLASS**  
  A class which you can use but never instantiate  
  ```java
  class node {
    private node () {
        System.out.println("here");
    }
    public static int fun(int x) {
        return x*x;
    }
  }
  ```
  This is possible since we dont have any INSTANCE VARIABLE. 
  Therefore we can very well use the function even without declaring 
  the functions  
  So we can never create object of math class
- Static methods cant use non-static variables/methods
- So `static` basically means something that is instantiated only once per class
  and will remain as it is for all the next invocation of `new` for the class 
  ```java
  class Duck {
    static int cnt = 0;
    public Duck() {
        cnt++;
    }
  }
  ```
- A **static initializer** is a block of code that runs when a class is loaded, 
  before any other code can use the class, so it’s a great place to initialize 
  a **static final variable**.
  ```java
  class Foo {
    final static int X; 
    static { 
        X = 42;
    }
  }
  ```
  `final static` variable can never be left uninitialised  
  `final method`: method can never be overridden  
  `final class`: can never be extended

- If required, `primitive` value can be changed to the `object` form by doing this
  ```java
  Integer iWrap = new Integer(i);
  ```

- overriding vs overloading  
  **overriding**
  - Arguments must be the same, and return
    types must be compatible.
  - The method can’t be less accessible.  
  - 
  **overloading**
  - An overloaded method is
    just a different method that happens to have the same method name. It has 
  nothing to do with inheritance and polymorphism. An overloaded method 
  is NOT the same as an overridden method

- A subclass inherits all public instance variables and methods of the 
superclass, but does not inherit the private instance variables and methods 
of the superclass.

- Inherited methods can be overridden; instance variables cannot be overridden

- If you declare an abstract method, you MUST mark the class abstract as well. 
You can’t have an abstract method in a non-abstract class.

- 

## Code snippets
## INTERFACE
- Interface in java   
  here functions are just declared not defined
  ```java 
  class Animal {
      public int myfun(int x) {
          return 0;
      }
  }

  interface Pet {
      void beFriendly();
  }

  class Dog extends Animal implements Pet {
      public void beFriendly() {
          System.out.println("yes i am friendly");
      }
  }

  public class Main {
      static void fun(Pet t) {
          t.beFriendly();
      }
      public static void main(String [] args) {
          System.out.println("satyam mishra");
          Dog tmp = new Dog();
          System.out.println(tmp.myfun(10));
          tmp.beFriendly();
          fun(tmp);
      }
      
  }
  ```
- Use `super` to get access to codes(attributes or methods) from the super class
  ```java
  class Dog extends Animal implements Pet {
      public void beFriendly() {
          System.out.println("yes i am friendly");
          System.out.println(super.x);
          System.out.println(x);
      }
  }
  ```

## CONSTRUCTOR
- in java constructor for all the super class are called when `new` is invocated
  ```java
  class node {
      public node () {
          System.out.println("here");
      }
  }
  
  class specialNode extends node {
      public specialNode() {
          System.out.println("inside special node");
      }
  }
  
  public class test {
      public static void main(String[] args) {
          specialNode tmp = new specialNode();
      }
  }
  ```
  OUTPUT
  ```
  here
  inside special node
  ```
- we can call parents constructor using super but 
if we dont then the compiler does it for us.
- we can pass arguments in the super class 
and the corresponding constructor is invoked in the parent class. 
- example 
  ```java
  public abstract class Animal { 
    private String name;
    public String getName() {
        return name; 
    }
    public Animal(String theName) {
        name = theName; 
    }
  }
  
  public class Hippo extends Animal {
    public Hippo(String name) {
        super(name); 
    }
  } 
  
  public class MakeHippo {
    public static void main(String[] args) {
        Hippo h = new Hippo(“Buffy”); 
        System.out.println(h.getName());
    }
  }
  ```

- **THIS KEYWORD**   
  - Use this() to call a constructor from another overloaded constructor in the same class.  
  - The call to this() can be used only in a constructor, and must be the first statement in a constructor.  
  - A constructor can have a call to super() OR this(), but never both!
 
- Example to make constructors clear
  ```java
  class StaticSuper {
    static {
      System.out.println(" super static block");
    }
  
    StaticSuper() {
      System.out.println(
              "super constructor");
    }
  }
  class StaticTests extends StaticSuper {
    static int rand;
    static {
      rand = (int) (Math.random() * 6);
      System.out.println("static block " + rand);
    }
    StaticTests() {
      System.out.println("constructor");
    }
    public static void main(String [] args) {
      System.out.println("in main");
      StaticTests st = new StaticTests();
    }
  }
  ```
  **OUTPUT**
  ```text
  super static block
  static block 0
  in main
  super constructor
  constructor
  ```

## LIFE OF VARIABLES

- A local variable lives only within the method that declared the variable.
- An instance variable lives as long as the object does. If the object is still alive, so are its instance variables.

## NUMBER FORMATTING
- inserting `,`'s in number
  ```java
  String s = String.format("%, d", 1000000000);
  ```

- setting precision
  ```java
  String s = String.format(“I have %.2f bugs to fix.”, 476578.09876);
  ```

- `%[argument number][flags][width][.precision]type`  
- for using previous argument
  ```java
  String.format(“%tA, %<tB %<td”,today);
  ```
- for time 
    - `%tc` = Sun Nov 28 14:52:41 MST 2004
    - `%tr` = 03:01:47 PM
    - `%tA, %tB %td` = Sunday, November 28

- For calendar related methods checkout book's page 304

## EXCEPTION HANDLING
### Throwing Error
```java
public void takeRisk() throws BadException {
    if (abandonAllHope) throw new BadException();
}
```
**NOTE**  :- if you use `thorws BadException`  then you dont need to handle it
the first class that has no `thow` need to handle Exception as shown below  
### Handling Error
```java
public void crossFingers() {
    try {
    anObject.takeRisk();
    } catch (BadException ex) {
        System.out.println("Aaargh !");
        ex.printStackTrace();
    }
}
```

- The compiler does NOT pay attention to exceptions that are of
  type `RuntimeException`.
- If there are mutliple catch blocks then the compiler will check them in 
  the order in which they are mentioned

## Saving the State
```java
import java.io.*;
public class Box implements Serializable {
  private int width;
  private int height;
  public void setWidth(int w) {
    width = w;
  }
  public void setHeight(int h) {
    height = h;
  }
  public static void main (String[] args) {
    Box myBox = new Box();
    myBox.setWidth(50);
    myBox.setHeight(20);
    try {
      FileOutputStream fs = new FileOutputStream(“foo.ser”);
      ObjectOutputStream os = new ObjectOutputStream(fs);
      os.writeObject(myBox);
      os.close();
    } catch(Exception ex) {
      ex.printStackTrace();
    }
  }
}
```

**For getting back the state**
```java
Object obj = os.readObject();
reqType reqObj = (reqType) obj;
```

- so if you want to make a class serializable then the whole tree of the object
and inside objects and the inside objects must be all serializable.
- using `implements Serializable` is necessary for a class to be able to be saved.
- so if we dont want to save something then we can use `transient` before 
  its declarating and it wont be saved

## FILE HANDLING

### Writing to a file
```java
try {
	FileWriter wtr = new FileWriter("testcase");
	wtr.write("Hello world");
	wtr.close();
}
catch (IOException ex) {
    ex.printStackTrace();
}
```
### File object
```java
// initialise a file object
File f = new File(“MyCode.txt”);

// intialise a directory
File dir = new File(“Chapter7”); 
dir.mkdir();

// list contents of a directory
if (dir.isDirectory()) {
    String[] dirContents = dir.list();
    for (int i = 0; i < dirContents.length; i++) {
        System.out.println(dirContents[i]); 
	}
}

// get absolute path
System.out.println(dir.getAbsolutePath());

// delete 
boolean isDeleted = f.delete();
```
- not actually a file but just a path to a file and we do all directory
  related stuff.

**Using buffer for reading files**
```java
BufferedWriter writer = new BufferedWriter(new FileWriter(aFile));
```

### Reading a file
```java
import java.io.*;
class ReadAFile {
	public static void main (String[] args) {
		try {
			File myFile = new File("MyText.txt");
			FileReader fileReader = new FileReader(myFile);

			BufferedReader reader = new BufferedReader(fileReader);

			String line;
			while ((line = reader.readLine()) != null)  System.out.println(line);
			reader.close();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
```

## Threads
```java
class MyRunnable implements Runnable {
	public void run() {
		System.out.println("top o’ the stack");
	}
}

class ThreadTestDrive {
	public static void main(String[] args) {
		Runnable threadJob = new MyRunnable();
		Thread myThread = new Thread(threadJob);
		myThread.start();
		try { Thread.sleep(2000);
		} catch(InterruptedException ex) { ex.printStackTrace(); }
		System.out.println("back in main");
	}
}
```
**OUTPUT**
```text
top o’ the stack
back in main
-------------------------
back in main
top o’ the stack
```
- so here the ouput can be any of the two. But when we add sleep in the code
main thread will go to sleep and therefore the schedule is forced to allow
MyRunnable to run.

- here any object that goes inside the `Thread` constructor has to 
implement `Runnable` and `Runnable` has just one function i.e. `run`.
- whenever a program starts, main thread is invocated. It may further call new
threads.

**MANY THREADS**
```java
class RunThreads implements Runnable {
	public static void main(String[] args) {
		RunThreads runner = new RunThreads();
		Thread alpha = new Thread(runner);
		Thread beta = new Thread(runner);
		alpha.setName("Alpha thread");
		beta.setName("Beta thread");
		alpha.start();
		try {
			Thread.sleep(1000);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		beta.start();
	}

	public void run() {
		for (int i = 0; i < 25; i++) {
			String threadName = Thread.currentThread().getName();
			System.out.println(threadName + " is running");
		}
	}
}
```

Here we can just simply use the `syncronized` keyword to make the increment method atomic.
```java
public synchronized void increment() 
```

Sometimes making the whole class `syncronized` might not be a good idea. So it is better to 
make only the section of the code `syncronized`.
```java
public void go () {
	doStuff();
	synchronized(this) {
	    criticalStuff();
		morecriticalStuff();
	}
}
```

**NOTE**  
Here `this` refers to the resource based on which we are taking the lock. 
so if this thing is same for two blocks then those two blocks will never be
executed simultaneously.

- Java automatically implements lock for the static variables present in the class (if 
you use some static methods on them)

## GENERICS
We can define generic type in java. This is same as templates in c++.  
We can use generics in the following manner in java.
```java
public class ArrayList<E> extends AbstractList<E> ... { public boolean add(E o)
```
But i dont know why but we cant simply call new on this type E. One obviuos work around is to 
ask the user to give me a method to create new type E objects. And I am not able to 
understand other ones.  
**PROBLEM**
```java
class MyClass<T>
{
    T field;
    public void myMethod()
    {
       field = new T(); // gives compiler error
    }
}
```

**SOLUTION**
```java
interface MyFactory<T> 
{
    T newObject();
}

class MyClass<T> 
{
    T field;
    public void myMethod(MyFactory<T> factory)
    {
       field = factory.newObject()
    }
}
```

If we want to use generics only for a perticular method not for the class as whole.
```java
public <T extends Animal> void takeThing(ArrayList<T> list)
```

Sort function in java collections
```java
public static <T extends Comparable<? super T>> void sort(List<T> list)
```

Here `extends` meand `extends` or `implements`. It doesnt matter.

And even `Comparable<? super T>` means that whatever `Comparable` this T extends must be 
a super class of T (including T itself).

```java
public interface Comparable<T> {
  int compareTo(T o);
}
```

Here we can just pass list object and it will use this internal comparator
```java
class Song implements Comparable<Song> { 
	String title;
	String artist;
	String rating;
	String bpm;
	public int compareTo(Song s) {
		return title.compareTo(s.getTitle());
	}
}
```

We can provide external comparator as well. 
```java
class ArtistCompare implements Comparator<Song> {
  public int compare(Song one, Song two) {
    return one.getArtist().compareTo(two.getArtist());
  }
}

ArtistCompare artistCompare = new ArtistCompare(); 
Collections.sort(songList, artistCompare);
```

If two objects have the same hashcode value, they are NOT required to be equal. 
But if they’re equal, they MUST have the same hashcode value.

**So, if you override equals(), you MUST override hashCode().**

We can't do something like this, and it makes sense as well

**IMPORTANT NOTE**  
Please dont confuse yourself with comparator or equals thing. Comparison may not be possible 
in every class but `isequal` is always possible. And this is the reason why `isequal` is part of 
object class.  
So where order is important like in case of `Collection.sort` or in case of `TreeSet` obviously we need
either `Comparator` or `Comparable` but in case where order is not important like in case of 
`HashSet` we dont need these. Only `equals` and `hashCode` are fine.  
And one more point to note is that equals take `Object` type as type of the other thing. And it
even makes sense as we can try to compare a Dog with its typecasted Animal form.
```java
Dog d = new Dog();
Animal da = (Animal)d; 
System.out.println(d.equals(da));
```
And one more thing is that `==` just compares the location in the memory and it has nothing to
do with `equals` or `hashCode`.


```java
public void go() {
  ArrayList<Dog> dogs = new ArrayList<Dog>();
  dogs.add(new Dog());
  dogs.add(new Dog());
  takeAnimals(dogs);
}

public void takeAnimals(ArrayList<Animal> animals) {
  for (Animal a : animals) {
	  a.eat();
  }
}
```

And this is not possible with arrays as well.  
**NOTE**
Array types are checked again at runtime, 
but collection type checks happen only when you compile

In order to make this thing work we can use wildcard as follows
```java
public void takeAnimals(ArrayList<? extends Animal> animals) {
    for(Animal a: animals) a.eat();
}
```

We can even use `T` here. It will give added advantages
```java
public <T extends Animal> void takeAnimals(ArrayList<T> animals, T nn) {
    for(Animal a: animals) a.eat();
    animals.add(nn);
}
```

So in `array` and `ArrayList` we can only use the exact same type on 
the LHS and the RHS. 