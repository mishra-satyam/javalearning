package testalgos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODOs
//2. add flags

public class grep {

	static final String ANSI_RED = "\u001B[31m";
	static final String ANSI_RESET = "\u001B[0m";

	public static void main(String[] args) {
		if (!(args.length == 2)) throw new AssertionError("usage: java Grep <word_to_search> <file_path>");

		String pat = args[0];
		String filePath  = args[1];

		File myFile = new File(filePath);
		// check if the given file exists
		if (!myFile.exists()) throw new AssertionError("Given file does not exists");

		try (
				FileReader fileReader = new FileReader(myFile);
				BufferedReader reader = new BufferedReader(fileReader)
				) {
			Pattern pattern = Pattern.compile(pat);

			String line;
			int lineNo = 0;
			while ((line = reader.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				int idx = 0;
				if (!matcher.find(idx)) {
					lineNo++;
					continue;
				}
				while (matcher.find(idx)) {
					for (int i = idx; i < matcher.start(); i++) System.out.print(line.charAt(i));
					System.out.print(ANSI_RED);
					for (int i = matcher.start(); i < matcher.end(); i++) System.out.print(line.charAt(i));
					System.out.print(ANSI_RESET);
					idx = matcher.end();
				}
				while (idx < line.length()) {
					System.out.print(line.charAt(idx));
					idx++;
				}
				System.out.println();
				lineNo++;
			}
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}

