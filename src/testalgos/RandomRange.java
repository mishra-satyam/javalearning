package testalgos;

import java.util.Random;

public class RandomRange {
	static Random rand = new Random();
	public static int valueInRange(int l, int r) {
		return rand.nextInt(r-l+1) + l;
	}
}
