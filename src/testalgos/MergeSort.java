package testalgos;
public class MergeSort {
    private MergeSort(){

    }
    private static void merge(int [] arr, int p, int q, int r)  {
        int l1 = q-p+1;
        int l2 = r-q;
        int [] left = new int [l1+1];
        int [] right = new int [l2+1];
        // q is taken in left segment
        for (int i = 0; i < l1; i++) left[i] = arr[i + p];
        for (int i = 0; i < l2; i++) right[i] = arr[i + q + 1];
        left[l1] = Integer.MAX_VALUE;
        right[l2] = Integer.MAX_VALUE;
        int p1 = 0, p2 = 0;
        for (int k = p; k <= r; k++) {
            if (left[p1] < right[p2]) {
                arr[k] = left[p1];
                p1++;
            }
            else {
                arr[k] = right[p2];
                p2++;
            }
        }
    }

    public static void sort(int [] arr, int l, int r) {
        if (l < r) {
            int mid =  (l+r)/2;
            sort(arr, l, mid);
            sort(arr, mid+1, r);
            merge(arr, l, mid, r);
        }
    }
}

class Main1 {
    public static void main(String[] args) {
        int [] tmp = new int [10];
        for (int i = 0; i < 10; i++) {
            tmp[i] = (int)(Math.random()*100);
        }

        for (int i = 0; i < 10; i++) {
            System.out.print(tmp[i] + " ");
        }
        System.out.println();

        MergeSort.sort(tmp, 0, 9);

        for (int i = 0; i < 10; i++) {
            System.out.print(tmp[i] + " ");
        }
        System.out.println();
    }
}