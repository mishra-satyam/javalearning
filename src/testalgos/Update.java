package testalgos;

public interface Update<TN, TU> {
	TU getInstance();

	void combine(TU other, int tl, int tr);

	void apply(TN x, int tl, int tr);
}
