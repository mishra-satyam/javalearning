package testalgos.RefSegTree;

public interface RNode {
	void merge(final RNode l, final RNode r);
}
