package testalgos.RefSegTree;

import testalgos.Node;
import testalgos.RandomRange;
import testalgos.SegTree;
import testalgos.Update;

import java.util.ArrayList;

public class RMain {
	public static void main(String[] args) {
		class MyNode implements RNode {
			int sum = 0;
			public MyNode(){}
			MyNode(int x)  {
				sum = x;
			}

			@Override
			public void merge(RNode l, RNode r) {
				sum = ((MyNode)l).sum + ((MyNode)r).sum;
			}
		}

		class MyUpdate implements RUpdate {
			int v = 0;
			public MyUpdate()  {}
			MyUpdate(int val) {
				v = val;
			}

			@Override
			public void combine(RUpdate other, int tl, int tr) {
				v += ((MyUpdate)other).v;
			}

			@Override
			public void apply(RNode x, int tl, int tr) {
				((MyNode)x).sum += v*(tr-tl+1);
			}
		}

		int [] arr = new int [10];
		for (int i = 0; i < 10; i++) {
			arr[i] = Math.round((float)(Math.random()*10));
		}


		for (int i = 0; i < 10; i++) {
			System.out.print(arr[i]  + " ");
		}
		System.out.println();

		ArrayList<MyNode> tmparr = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			tmparr.add(new MyNode(arr[i]));
		}
		// segtree object
		RefSegTree<MyNode, MyUpdate> obj = new RefSegTree<>(10, MyNode.class, MyUpdate.class);
		obj.build(tmparr);



		// random number generator

		// check range sum
		int l, r, sum;
		l = RandomRange.valueInRange(0, 9);
		r = RandomRange.valueInRange(l,9);
		sum = 0;
		System.out.println("range sum case: l = " + l + ", r = " + r );
		for (int i = l; i <= r; i++) {
			sum += arr[i];
		}
		System.out.println(sum);
		System.out.println(obj.query(l, r).sum);

		// check range update
		l = RandomRange.valueInRange(0, 9);
		r = RandomRange.valueInRange(l,9);
		sum = RandomRange.valueInRange(0, 100);

		System.out.println("range update case: l = " + l + ", r = " + r + ", update = " + sum);

		for (int i = l; i <= r; i++) {
			arr[i] += sum;
		}

		for (int i = 0; i < 10; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		MyUpdate upd = new MyUpdate(sum);
		obj.rupd(l, r, upd);

//		tmparr.get(1).sum = 100;
//		System.out.println(tmparr.get(1).sum);

		for (int i = 0; i < 10; i++) {
			System.out.print(obj.query(i,i).sum + " ");
		}
		System.out.println();
	}
}
