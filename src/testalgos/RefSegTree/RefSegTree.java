package testalgos.RefSegTree;

import testalgos.Node;
import testalgos.Update;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class RefSegTree<N extends RNode, U extends RUpdate> {
	int len;
	ArrayList<N> t = new ArrayList<>();
	ArrayList<U> u = new ArrayList<>();
	ArrayList<Boolean> lazy = new ArrayList<>();
	N identity_element;
	Constructor<N> nodeGenerator;
	Constructor<U> updateGenerator;
	RefSegTree(int l, Class<N> node, Class<U> update) {
		len = l;
		try {
			nodeGenerator = node.getConstructor();
			updateGenerator = update.getConstructor();
			for (int i = 0; i < 4 * l; i++) t.add(nodeGenerator.newInstance());
			for (int i = 0; i < 4 * l; i++) u.add(updateGenerator.newInstance());
			for (int i = 0; i < 4 * l; i++) lazy.add(false);
			identity_element = nodeGenerator.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private void pushdown(int v, int tl, int tr) {
		if (!lazy.get(v)) return;
		int tm = (tl+tr)>>1;
		apply(v<<1, tl, tm, u.get(v));
		apply(v<<1|1, tm+1, tr, u.get(v));
		try {
			u.set(v, updateGenerator.newInstance());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		lazy.set(v, false);
	}

	private void apply(int v, int tl, int tr, U upd){
		if(tl != tr){
			lazy.set(v, true);
			u.get(v).combine(upd,tl,tr);
		}
		upd.apply(t.get(v),tl,tr);
	}

	private void build(ArrayList<N> arr,int v, int tl, int tr){
		if(tl == tr){
			t.set(v, arr.get(tl));
			return;
		}
		int tm = (tl + tr) >> 1;
		build(arr,v<<1,tl,tm);
		build(arr,v<<1|1,tm+1,tr);
		t.get(v).merge(t.get(v<<1),t.get(v<<1|1));
	}

	private N query(int v,int tl,int tr,int l,int r) {
		if(l > tr || r < tl){
			return identity_element;
		}
		if(tl >= l && tr <= r){
			return t.get(v);
		}
		pushdown(v,tl,tr);
		int tm = (tl + tr) >> 1;
		N a = query(v<<1,tl,tm,l,r),b = query(v<<1|1,tm+1,tr,l,r);
		N ans;
		try {
			ans = nodeGenerator.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		ans.merge(a,b);
		return ans;
	}

	private void rupd(int v,int tl,int tr,int l,int r,U upd){
		if(l > tr || r < tl){
			return;
		}
		if(tl >= l && tr <= r){
			apply(v,tl,tr,upd);
			return;
		}
		pushdown(v,tl,tr);
		int tm = (tl + tr) >> 1;
		rupd(v<<1,tl,tm,l,r,upd);
		rupd(v<<1|1,tm+1,tr,l,r,upd);
		t.get(v).merge(t.get(v<<1),t.get(v<<1|1));
	}

	public void build (ArrayList<N> arr) {
		build(arr, 1, 0, len-1);
	}

	public N query(int l, int r) {
		return query(1, 0, len-1, l, r);
	}

	public void rupd(int l, int r, U upd) {
		rupd(1, 0, len-1, l, r, upd);
	}
}


