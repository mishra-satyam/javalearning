package testalgos.RefSegTree;

public interface RUpdate {
	void combine(RUpdate other, int tl, int tr);

	void apply(RNode x, int tl, int tr);
}
