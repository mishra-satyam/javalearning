# My Observations 

## Grep
Time of execution comparison  
For my grep code :
```bash
time java Grep th. ../../sample.txt
```
**OUPUT**
```text
real    0m0.923s
user    0m0.754s
sys     0m0.220s
```

For grep:
```bash
time Grep th. ../../sample.txt
```
**OUPUT**
```text
real	0m0.009s
user	0m0.004s
sys	0m0.004s
```