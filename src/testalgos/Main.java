package testalgos;

import java.util.ArrayList;

public class Main {
	static int x = 100;
	int tmp = 1000;
	public static void main(String[] args) {

		class MyNode implements Node<MyNode> {
			int sum = 0;
			MyNode(){}
			MyNode(int x)  {
				sum = x;
			}

			@Override
			public MyNode getInstance() {
				return new MyNode();
			}

			@Override
			public void merge(MyNode l, MyNode r) {
				sum = l.sum + r.sum;
			}
		}

		class MyUpdate implements Update<MyNode, MyUpdate> {
			int v = 0;
			MyUpdate()  {}
			MyUpdate(int val) {
				v = val;
			}

			@Override
			public MyUpdate getInstance() {
				return new MyUpdate();
			}

			@Override
			public void combine(MyUpdate other, int tl, int tr) {
				v += other.v;
			}

			@Override
			public void apply(MyNode x, int tl, int tr) {
				x.sum += v*(tr-tl+1);
			}
		}

		int [] arr = new int [10];
		for (int i = 0; i < 10; i++) {
			arr[i] = Math.round((float)(Math.random()*10));
		}


		for (int i = 0; i < 10; i++) {
			System.out.print(arr[i]  + " ");
		}
		System.out.println();

		ArrayList<MyNode> tmparr = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			tmparr.add(new MyNode(arr[i]));
		}
		// segtree object
		SegTree<MyNode, MyUpdate> obj = new SegTree<>(10, new MyNode(), new MyUpdate());
		obj.build(tmparr);



		// random number generator

		// check range sum
		int l, r, sum;
		l = RandomRange.valueInRange(0, 9);
		r = RandomRange.valueInRange(l,9);
		sum = 0;
		System.out.println("range sum case: l = " + l + ", r = " + r );
		for (int i = l; i <= r; i++) {
			sum += arr[i];
		}
		System.out.println(sum);
		System.out.println(obj.query(l, r).sum);

		// check range update
		l = RandomRange.valueInRange(0, 9);
		r = RandomRange.valueInRange(l,9);
		sum = RandomRange.valueInRange(0, 100);

		System.out.println("range update case: l = " + l + ", r = " + r + ", update = " + sum);

		for (int i = l; i <= r; i++) {
			arr[i] += sum;
		}

		for (int i = 0; i < 10; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		MyUpdate upd = new MyUpdate(sum);
		obj.rupd(l, r, upd);

//		tmparr.get(1).sum = 100;
//		System.out.println(tmparr.get(1).sum);

		for (int i = 0; i < 10; i++) {
			System.out.print(obj.query(i,i).sum + " ");
		}
		System.out.println();

	}

}