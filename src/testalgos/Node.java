package testalgos;

public interface Node<T> {
	// giving user to implement this function may arise problems as then he might try to do fishy things.
	T getInstance();

	void merge(final T l, final T r);
}

