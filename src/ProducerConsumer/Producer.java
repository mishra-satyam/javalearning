package ProducerConsumer;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

class ProducerThread implements Runnable{
	private boolean stop;
	private final Thread t;
	private final InterMediator interMediator;
	ProducerThread(InterMediator interMediatorVal) {
		t = new Thread(this);
		stop = false;
		interMediator = interMediatorVal;
	}

	public void start() {t.start();}

	@Override
	public void run() {
		while (!stop) {
			int val = interMediator.getUpdateCounter();
			Resource resource = new Resource(val);
			boolean b  = interMediator.setResource(resource);
			if (!b) return;
			System.err.println(t.getName() + ": resource number " + val + " produced");
		}
	}

	public void kill() {
		stop = true;
	}
}

public class Producer {
	private int cnt;
	private final Deque<ProducerThread> threads;
	private final InterMediator interMediator;

	public Producer (InterMediator interMediatorVal, int cntVal) {
		cnt = cntVal;
		interMediator = interMediatorVal;
		threads = new LinkedList<>();
		for (int i = 0; i < cntVal; i++) {
			ProducerThread t = new ProducerThread(interMediator);
			threads.addLast(t);
			t.start();
		}
	}

	public int getProducersCount() {
		return threads.size();
	}

	public void increaseProducers(int val) {
		cnt += val;
		for (int i = 0; i < val; i++) {
			ProducerThread t = new ProducerThread(interMediator);
			threads.addLast(t);
			t.start();
		}
	}

	public void decreaseProducers(int val) {
		if (val > cnt) {
			System.out.println("Cant decrease these many producers");
			return;
		}
		cnt -= val;
		for (int i = 0; i < val; i++) {
			ProducerThread t = threads.removeFirst();
			t.kill();
		}
	}
}