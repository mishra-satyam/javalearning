package ProducerConsumer;

public class Resource {
	int preciousValue;

	public Resource(){
		super();
	}
	public Resource(int preciousValue) {
		this.preciousValue = preciousValue;
	}

	public int getPreciousValue() {
		return preciousValue;
	}

	public void setPreciousValue(int preciousValue) {
		this.preciousValue = preciousValue;
	}
}
