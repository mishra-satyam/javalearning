package ProducerConsumer;

import java.util.LinkedList;

class ConsumerThread implements Runnable{
	private boolean stop;
	private final Thread t;
	private final InterMediator interMediator;
	ConsumerThread(InterMediator interMediatorVal) {
		stop = false;
		t = new Thread(this);
		interMediator = interMediatorVal;
	}

	public void start() {
		t.start();
	}

	@Override
	public void run() {
		Resource r;
		while ((!stop) && ((r = interMediator.getResource()) != null)) {
			System.err.println(t.getName() + ": resource number " + r.preciousValue + " consumed");
		}
	}

	public void kill() {
		stop = true;
	}
}

public class Consumer {
	private int cnt;
	private final LinkedList<ConsumerThread> threads;
	private final InterMediator interMediator;

	public Consumer (InterMediator interMediatorVal, int cntVal) {
		cnt = cntVal;
		interMediator = interMediatorVal;
		threads = new LinkedList<>();
		for (int i = 0; i < cntVal; i++) {
			ConsumerThread t = new ConsumerThread(interMediator);
			threads.addLast(t);
			t.start();
		}
	}

	public int getConsumersCount() {
		return threads.size();
	}

	public void increaseConsumers(int val) {
		cnt += val;
		for (int i = 0; i < val; i++) {
			ConsumerThread t = new ConsumerThread(interMediator);
			threads.addLast(t);
			t.start();
		}
	}

	public void decreaseConsumer(int val) {
		if (val > cnt) {
			System.out.println("Cant decrease these many consumers");
			return;
		}
		cnt -= val;
		for (int i = 0; i < val; i++) {
			ConsumerThread t = threads.removeFirst();
			t.kill();
		}
	}
}