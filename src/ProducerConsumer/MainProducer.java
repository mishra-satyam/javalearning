package ProducerConsumer;

import java.util.Scanner;

public class MainProducer {

	public static void main(String[] args) {
		InterMediator interMediator = new InterMediator();

		Producer producer = new Producer(interMediator, 2);

//		Consumer consumer = new Consumer(interMediator, 2);

		int val = -1;
		Scanner myObj = new Scanner(System.in);
		while (true) {
			System.out.println("Enter 1 to stop all producer\n" +
					"Else enter 2 and then the change in number of producers you want to make");
			System.out.println("Currently there are " + producer.getProducersCount() + " producers");
			val = myObj.nextInt();
			if (val == 1) {
				interMediator.stop();
				return;
			}
			else if (val == 2) {
				val = myObj.nextInt();
				if (val >= 0) producer.increaseProducers(val);
				else producer.decreaseProducers(-val);
			}
			else {
				System.out.println("please enter either 1 or 2");
			}
		}

	}
}
