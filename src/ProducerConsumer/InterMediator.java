package ProducerConsumer;

import com.mongodb.MongoClientSettings;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class InterMediator {
	private boolean alive;
	static private int counter;
	private final MongoCollection<Resource> collection;
	InterMediator() {
		alive = true;

		Logger logger = Logger.getLogger("org.mongodb.driver");
		logger.setLevel(Level.WARNING);

		String mongoUri = URI.uri;
		String databaseName = "resources";
		String collectionName = "savedResources";
		MongoClient mongoClient = MongoClients.create(mongoUri);

		MongoDatabase database = mongoClient.getDatabase(databaseName);
		CodecRegistry pojoCodecRegistry =
				fromRegistries(
						MongoClientSettings.getDefaultCodecRegistry(),
						fromProviders(PojoCodecProvider.builder().automatic(true).build()));

		collection = database.getCollection(collectionName, Resource.class).withCodecRegistry(pojoCodecRegistry);
	}

	public int getUpdateCounter() {
		synchronized (this) {
			int currentCount = counter;
			counter++;
			return currentCount;
		}
	}

	public Resource getResource() {
		Resource resource;
		synchronized (this) {
			while(((resource = collection.findOneAndDelete(new Document()))==null) && alive) {
				try {
					System.err.println(Thread.currentThread().getName() + ": Waiting for producer to produce something");
					this.wait(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return resource;
		}
	}

	public boolean setResource(Resource r) {
		if (!alive) return false;
		synchronized (this) {
			collection.withWriteConcern(WriteConcern.MAJORITY).insertOne(r);
			this.notify();
		}
		return true;
	}

	public void stop() {
		alive = false;
	}
}