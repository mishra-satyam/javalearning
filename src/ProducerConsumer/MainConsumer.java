package ProducerConsumer;

import java.util.Scanner;

public class MainConsumer {

	public static void main(String[] args) {
		InterMediator interMediator = new InterMediator();

//		Producer producer = new Producer(interMediator, 2);

		Consumer consumer = new Consumer(interMediator, 2);

		int val = -1;
		Scanner myObj = new Scanner(System.in);
		while (true) {
			System.out.println("Enter 1 to stop all consumers\n" +
					"Else enter 2 and then the change in number of consumers you want to make");
			System.out.println("Currently there are " + consumer.getConsumersCount() + " consumers");
			val = myObj.nextInt();
			if (val == 1) {
				interMediator.stop();
				return;
			}
			else if (val == 2) {
				val = myObj.nextInt();
				if (val >= 0) consumer.increaseConsumers(val);
				else consumer.decreaseConsumer(-val);
			}
			else {
				System.out.println("please enter either 1 or 2");
			}
		}
	}
}
