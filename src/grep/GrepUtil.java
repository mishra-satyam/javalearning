package grep;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

public class GrepUtil {
	public static void getFiles(File f, ArrayList<File> files) {
		if (f.getName().charAt(0) == '.') return;
		if (f.isFile()) {
			files.add(f);
			return;
		}
		for (File newF :
				Objects.requireNonNull(f.listFiles())) {
			getFiles(newF, files);
		}
	}
}
