package grep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODOs
//2. add flags

public class Grep {

	static final String ANSI_RED = "\u001B[31m";
	static final String ANSI_BLUE = "\u001B[34m";

	static final String ANSI_RESET = "\u001B[0m";

	public static void search(String pat, String filePath) {
		search(pat, filePath, filePath);
	}
	public static void search(String pat, String filePath, String topFilePath) {

		File myFile = new File(filePath);
		File topFile = new File(topFilePath);

		// check if the given file exists
		if (!myFile.exists()) throw new AssertionError(myFile.getAbsolutePath() + ": file does not exists");

		// if hidden then dont access
		if (myFile.getName().charAt(0) == '.') return;

		if (myFile.isDirectory()) {
			for (File f :
					Objects.requireNonNull(myFile.listFiles()))
				Grep.search(pat, f.getAbsolutePath(), topFilePath);
			return;
		}

		int cnt = 0;

		try (
				FileReader fileReader = new FileReader(myFile);
				BufferedReader reader = new BufferedReader(fileReader)
				) {


			String line;
			while ((line = reader.readLine()) != null) {
				int idx = 0;
				if (line.indexOf(pat, idx) == -1) {
					continue;
				}
				cnt++;
//				while (line.indexOf(pat, idx) != -1) {
//					cnt++;
//					idx = line.indexOf(pat, idx)+pat.length();
//				}
//				while (idx < line.length()) {
//					System.out.print(line.charAt(idx));
//					idx++;
//				}
			}
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}

		System.out.println(myFile.getName()+ " " + cnt);
	}
}

