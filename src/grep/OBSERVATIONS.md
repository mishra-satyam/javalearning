# Observations

## Time of execution (Grep vs Non-Thread vs Thread)
Data for testing consists of 20 files of size 9.5 MB each.

- Using `grep`
    ```text
  Satyams-MacBook-Pro:grep satyam$ time grep th /Users/satyam/IdeaProjects/learningJava/greptest -rc
  /Users/satyam/IdeaProjects/learningJava/greptest/f20:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f18:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f11:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f16:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f17:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f10:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f19:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/test:5
  /Users/satyam/IdeaProjects/learningJava/greptest/f4:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f3:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f2:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f5:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f15:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f12:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f13:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f14:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f9:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f7:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f6:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f1:44660
  /Users/satyam/IdeaProjects/learningJava/greptest/f8:44660
  
  real	0m0.936s
  user	0m0.880s
  sys	0m0.053s
    ```
- Non-thread case
  ```text
  Satyams-MacBook-Pro:grep satyam$ time java grep.Main th /Users/satyam/IdeaProjects/learningJava/greptest 0 15
  f20 44660
  f18 44660
  f11 44660
  f16 44660
  f17 44660
  f10 44660
  f19 44660
  test 5
  f4 44660
  f3 44660
  f2 44660
  f5 44660
  f15 44660
  f12 44660
  f13 44660
  f14 44660
  f9 44660
  f7 44660
  f6 44660
  f1 44660
  f8 44660
  
  real	0m1.457s
  user	0m1.961s
  sys	0m0.162s
  ```

- Thread case
  ```text
  Satyams-MacBook-Pro:grep satyam$ time java grep.Main th /Users/satyam/IdeaProjects/learningJava/greptest 2 20
  test 5
  f16 44660
  f4 44660
  f18 44660
  f1 44660
  f12 44660
  f20 44660
  f10 44660
  f3 44660
  f8 44660
  f15 44660
  f19 44660
  f5 44660
  f6 44660
  f14 44660
  f11 44660
  f13 44660
  f2 44660
  f9 44660
  f7 44660
  f17 44660
  
  real	0m0.659s
  user	0m3.928s
  sys	0m0.275s
  ```