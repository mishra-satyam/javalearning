package grep;

public class Main {
	public static void main(String[] args) {
		if (!(args.length == 4)) throw new AssertionError("usage: java Grep <pattern_to_search> <file_path> " +
				"<number of threads> <print whole lines>");

		String pat = args[0];
		String filePath = args[1];

		int cnt = Integer.parseInt(args[2]);
		int t = Integer.parseInt(args[3]);

		TPGrep.search(pat, filePath, cnt, t);
	}
}
