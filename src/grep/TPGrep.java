package grep;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class MyNewRunnable implements Runnable {
	private final File file;
	private final String pattern;
	private final BufferedWriter myout;
	private final int wholeLines;
	private final Map<String, Integer> map;
	MyNewRunnable (File fileIn, String pat, BufferedWriter out, int t, Map<String, Integer> m) {
		file = fileIn;
		pattern = pat;
		myout = out;
		wholeLines = t;
		map = m;
	}
	public void run() {
		TPGrep.searchFile(file, pattern, myout, wholeLines, map);
	}
}


public class TPGrep {

	static final String ANSI_RED = "\u001B[31m";
	static final String ANSI_BLUE = "\u001B[34m";

	static final String ANSI_RESET = "\u001B[0m";

	public static void search(String pat, String filePath, int n, int t) {

		File myFile = new File(filePath);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));

		// check if the given file exists
		if (!myFile.exists()) throw new AssertionError(myFile.getAbsolutePath() + ": file does not exists");

		// getting all the files in a list
		ArrayList<File> fileList = new ArrayList<>();
		GrepUtil.getFiles(myFile, fileList);

		Map<String, Integer> map = new HashMap<>();


//        WITH BUILT-IN THREADPOOL EXECUTOR
//		ExecutorService service = Executors.newFixedThreadPool(n);
//
//		for (File file : fileList) {
//			service.execute(new MyNewRunnable(file, pat, out, t, map));
//		}
//
//		service.shutdown();


		ThreadPool service = ThreadPool.newFixedThreadPool(n);

		for (File file :
				fileList) {
			service.execute(new MyNewRunnable(file, pat, out, t, map));
		}

		service.shutdown();

		while (!service.isTerminated()) {}

		if (t == 0) {
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				System.out.println(entry.getKey() + ": " + entry.getValue());
			}
		}
		try {
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}




	static void searchFile(File tmpFile, String pat, BufferedWriter out, int wholeLines, Map<String, Integer> map) {
		int matchCount = 0;
		int lines = 0;

//		OLD IO
		try (
				FileReader fileReader = new FileReader(tmpFile);
				BufferedReader reader = new BufferedReader(fileReader)
		) {

			Pattern pattern = Pattern.compile(pat);
			String line;
			while ((line = reader.readLine()) != null) {

//      USING NEW IO
//		try {
//
//			Pattern pattern = Pattern.compile(pat);
//
//			List<String> allLines = Files.readAllLines(Paths.get(tmpFile.getAbsolutePath()));
//
//			for(String line : allLines) {

				Matcher matcher = pattern.matcher(line);
				int idx = 0;
				if (!matcher.find(idx)) {
					continue;
				}

				lines++;
				if (wholeLines == 1) {
					synchronized (ANSI_RESET) {
						out.write(/*ANSI_BLUE + */
								tmpFile.getAbsolutePath() + ": "
						       /*+ ANSI_RESET*/);
						out.write(line);
						out.write("\n");
					}
				}
				else {
					while (matcher.find(idx)) {
						matchCount++;
						idx = matcher.end();
					}
				}

			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		map.put(tmpFile.getAbsolutePath(), matchCount);
	}

}

