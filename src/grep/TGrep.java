package grep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODOs
// instead of making the whole block synchronized,
// split that to cover just the print statements

class MyRunnable implements Runnable {
	ArrayList<File> files;
	String pattern;
	MyRunnable (ArrayList<File> fileList, String pat) {
		files = fileList;
		pattern = pat;
	}
	public void run() {
		for (File f :
				files) {
			TGrep.searchFile(f, pattern);
		}
	}
}


public class TGrep {

	static final String ANSI_RED = "\u001B[31m";
	static final String ANSI_BLUE = "\u001B[34m";

	static final String ANSI_RESET = "\u001B[0m";

	public static void search(String pat, String filePath, int n) {

		File myFile = new File(filePath);

		// check if the given file exists
		if (!myFile.exists()) throw new AssertionError(myFile.getAbsolutePath() + ": file does not exists");

		// getting all the files in a list
		ArrayList<File> fileList = new ArrayList<>();
		GrepUtil.getFiles(myFile, fileList);

		//	total files = tot
		//	threads = n
		//	v = tot/n
		//	tot%n will get v+1
		//	rest v

		int tot = fileList.size();
		int idx = 0;
		for (int i = 0; i < n; i++) {
			ArrayList<File> tmp = new ArrayList<>();
			int lim = tot/n;
			if (i < tot%n) lim++;
			for (int j = 0; j < lim; j++) {
				tmp.add(fileList.get(idx++));
			}
			Runnable threadJob = new MyRunnable(tmp, pat);
			Thread myThread = new Thread(threadJob);
			myThread.start();
		}
	}




	public static void searchFile(File tmpFile, String pat) {
		try (
				FileReader fileReader = new FileReader(tmpFile);
				BufferedReader reader = new BufferedReader(fileReader)
		) {

			Pattern pattern = Pattern.compile(pat);

			String line;
			while ((line = reader.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				int idx = 0;
				if (!matcher.find(idx)) {
					continue;
				}

				synchronized(ANSI_RESET) {
					System.out.print(ANSI_BLUE
							+ tmpFile.getAbsolutePath() + ": " +
							ANSI_RESET);

					while (matcher.find(idx)) {
						for (int i = idx; i < matcher.start(); i++) System.out.print(line.charAt(i));
						System.out.print(ANSI_RED);
						for (int i = matcher.start(); i < matcher.end(); i++) System.out.print(line.charAt(i));
						System.out.print(ANSI_RESET);
						idx = matcher.end();
					}
					while (idx < line.length()) {
						System.out.print(line.charAt(idx));
						idx++;
					}
					System.out.println();
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}

