package grep;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ThreadPool {
	private LinkedList<Runnable> taskQueue;
	private List<Thread> threads;
	private boolean alive;

	private ThreadPool() {}

	public static ThreadPool newFixedThreadPool(int n)  {
		ThreadPool newPool = new ThreadPool();
		newPool.taskQueue = new LinkedList<>();
		newPool.threads =  new ArrayList<>();
		newPool.alive = true;
		for (int i = 0; i < n; i++) {
			MyNewTPRunnable tmp = new MyNewTPRunnable(newPool);
			Thread task = new Thread(tmp);
			newPool.threads.add(task);
			task.start();
		}
		return newPool;
	}

	public void execute(Runnable r) {
		synchronized (this) {
			taskQueue.addLast(r);
			this.notify();
		}
	}

	public void shutdown() {
		synchronized (this) {
			alive = false;
			this.notifyAll();
		}
	}

	public boolean isTerminated() {
		for (Thread t :
				threads) {
			if (t.isAlive()) return false;
		}
		return true;
	}


	static class MyNewTPRunnable implements Runnable {
		final ThreadPool pool;
		MyNewTPRunnable (ThreadPool newPool) {
			pool = newPool;
		}
		public void run() {
			Runnable r;
			while (true) {
				synchronized (pool) {
					while(pool.taskQueue.isEmpty() && pool.alive) {
						try {
							pool.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					if (!pool.taskQueue.isEmpty()) r = pool.taskQueue.removeFirst();
					else return;
				}
				r.run();
			}
		}
	}
}
