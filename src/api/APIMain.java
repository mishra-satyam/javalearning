package api;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



public class APIMain {
	public static void main(String[] args) throws IOException, InterruptedException {
		String handle = "Tetraxx";

		var url = "https://codeforces.com/api/user.rating?handle=" + handle;

		var request = HttpRequest.newBuilder().GET().uri(URI.create(url)).build();

		var client = HttpClient.newBuilder().build();

		var response = client.send(request, HttpResponse.BodyHandlers.ofString());

		System.out.println(response.body());
		System.out.println(response.statusCode());

		String jsonString = response.body();
		System.out.println(jsonString);

		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();

		Gson gson = builder.create();
		CFReturn data = gson.fromJson(jsonString, CFReturn.class);
		System.out.println(data);

		jsonString = gson.toJson(data);
		System.out.println(jsonString);

		System.out.println("Best rank " + data.bestRank());

		System.out.println("Best Rating " + data.getHighestRating());
	}
}
