package api;

import grep.Main;

import java.util.List;

class Contests{
	private int contestId;
	private String contestName;
	private String handle;
	private int rank;
	private int ratingUpdateTimeSeconds;
	private int oldRating;
	private int newRating;

	public int getContestId() {
		return contestId;
	}

	public void setContestId(int contestId) {
		this.contestId = contestId;
	}

	public String getContestName() {
		return contestName;
	}

	public void setContestName(String contestName) {
		this.contestName = contestName;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getRatingUpdateTimeSeconds() {
		return ratingUpdateTimeSeconds;
	}

	public void setRatingUpdateTimeSeconds(int ratingUpdateTimeSeconds) {
		this.ratingUpdateTimeSeconds = ratingUpdateTimeSeconds;
	}

	public int getOldRating() {
		return oldRating;
	}

	public void setOldRating(int oldRating) {
		this.oldRating = oldRating;
	}

	public int getNewRating() {
		return newRating;
	}

	public void setNewRating(int newRating) {
		this.newRating = newRating;
	}
}

public class CFReturn {
	private String status;
	private String comment;
	private List<Contests> result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<Contests> getResult() {
		return result;
	}

	public void setResult(List<Contests> result) {
		this.result = result;
	}

	public int getHighestRating() {
		int mx = 0;
		for (Contests c :
				result) {
			mx = Math.max(mx, c.getNewRating());
		}
		return mx;
	}

	public int bestRank() {
		int mn = Integer.MAX_VALUE;
		for (Contests c :
				result) {
			mn = Math.min(mn, c.getRank());
		}
		return mn;
	}
}
